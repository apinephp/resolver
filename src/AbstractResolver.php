<?php
/**
 * APIne Dependency Resolver
 *
 * @link      https://gitlab.com/apinephp/resolver
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/resolver/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Resolver;

use Psr\Container\ContainerInterface;

/**
 * Class AbstractResolver
 */
abstract class AbstractResolver
{
    /**
     * @var \Psr\Container\ContainerInterface
     */
    private ContainerInterface $container;
    
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
    
    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }
}