<?php
/**
 * APIne Dependency Resolver
 *
 * @link      https://gitlab.com/apinephp/resolver
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/resolver/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);


/**
 * Class ObjectConstructorResolverTestClass
 */
class ObjectConstructorResolverTestClass {
    public $request;
    
    public function __construct(TestClassInterface $request)
    {
        $this->request = $request;
    }
}