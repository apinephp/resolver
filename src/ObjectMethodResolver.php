<?php
/**
 * APIne Dependency Resolver
 *
 * @link      https://gitlab.com/apinephp/resolver
 * @copyright Copyright (c) 2018-2019 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/resolver/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Resolver;

use InvalidArgumentException;
use Psr\Container\ContainerInterface;
use ReflectionException;
use ReflectionMethod;
use RuntimeException;
use function is_string, get_class;

/**
 * Class ObjectMethodResolver
 *
 * @package Apine\Resolver
 */
class ObjectMethodResolver extends AbstractResolver implements ObjectMethodResolverInterface
{
    use ArgumentsAwareTrait;
    
    /**
     * @var object
     */
    private object $object;
    
    /**
     * ObjectMethodResolver constructor.
     *
     * @param \Psr\Container\ContainerInterface $container
     * @param class-string|object               $object
     *
     * @throws \ReflectionException
     * @throws \Throwable
     */
    public function __construct(ContainerInterface $container, $object) {
        parent::__construct($container);
        $this->setObject($object);
    }
    
    public function getObject(): object
    {
        return $this->object;
    }
    
    /**
     * @param class-string|object $object
     *
     * @throws \ReflectionException
     * @throws \Throwable
     */
    public function setObject($object): void
    {
        $resolver = new ObjectConstructorResolver($this->getContainer());
        $this->object = $resolver->resolve($object);
    }
    
    /**
     * @param string $toResolve
     *
     * @return mixed
     */
    public function resolve($toResolve)
    {
        if (!is_string($toResolve)) {
            throw new InvalidArgumentException('Method name must be a string');
        }
        
        try {
            $reflection = new ReflectionMethod($this->object, $toResolve);
            $arguments = $this->parseArguments($this->getContainer(), $reflection);
            return $reflection->invokeArgs($this->object, $arguments);
        } catch (ReflectionException $e) {
            throw new RuntimeException(sprintf('Method %s not found in class %s', $toResolve, get_class($this->object)));
        }
        
        
    }
}