Dependency Resolver
===================

A IoC Dependency Resolver

Resolve dependencies of functions, methods, and constructors from a DI container 

Installation
------------

Installation is made with composer

```sh
composer require apinephp/resolver
```

The package requires PHP 7.4 or newer.

Usage Example
-------------

```php
<?php

use Apine\Resolver\CallableResolver;
use Apine\Resolver\ObjectConstructorResolver;
use Apine\Resolver\ObjectMethodResolver;

// Create an instance of a class
$resolver = new ObjectConstructorResolver($container);
$object = $resolver->resolve(StubClass::class);

// Execute a method
$resolver = new ObjectMethodResolver($container, $object);
$result = $resolver->resolve('method');

// Call a function
$resolver = new CallableResolver($container);
$result = $resolver->resolve('functionName');
```
