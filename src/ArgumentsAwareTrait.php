<?php
/**
 * APIne Dependency Resolver
 *
 * @link      https://gitlab.com/apinephp/resolver
 * @copyright Copyright (c) 2018-2019 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/resolver/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Resolver;

use Psr\Container\ContainerInterface;
use ReflectionFunctionAbstract;

/**
 * Trait ArgumentsAwareTrait
 *
 * @package Apine\Resolver
 */
trait ArgumentsAwareTrait
{
    /**
     * @var array<string,mixed>
     */
    private array $arguments = [];
    
    /**
     * @var mixed
     */
    private $defaultValue;
    
    /**
     * @return array<string,mixed>
     */
    public function getArguments(): array
    {
        return $this->arguments;
    }
    
    /**
     * @param array<string,mixed> $arguments
     */
    public function setArguments(array $arguments): void
    {
        $this->arguments = $arguments;
    }
    
    /**
     * @return mixed
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }
    
    /**
     * @param mixed $value
     *
     * @noinspection PhpUnused
     */
    public function setDefaultValue($value): void
    {
        $this->defaultValue = $value;
    }
    
    /**
     * @param string $name
     *
     * @return bool
     */
    public function hasArgument(string $name): bool
    {
        return isset($this->arguments[$name]);
    }
    
    /**
     * @param string $name
     *
     * @return mixed|null
     */
    public function getArgument(string $name)
    {
        return $this->hasArgument($name) ? $this->arguments[$name] : $this->defaultValue;
    }
    
    /**
     * @param \Psr\Container\ContainerInterface $container
     * @param \ReflectionFunctionAbstract       $reflector
     *
     * @return array
     * @throws \ReflectionException
     */
    private function parseArguments(ContainerInterface $container, ReflectionFunctionAbstract $reflector): array
    {
        $arguments = [];
        foreach ($reflector->getParameters() as $parameter) {
            $name = $parameter->getName();
            $defaultValue = $parameter->isDefaultValueAvailable() ?
                $parameter->getDefaultValue() :
                $this->getDefaultValue();
    
            $value = $this->getArgument($name) ?? $defaultValue;
            
            if ($container->has($name)) {
                $value = $container->get($name);
            }
            
            $arguments[$name] = $value;
        }
        
        return $arguments;
    }
}