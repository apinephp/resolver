<?php
/**
 * APIne Dependency Resolver
 *
 * @link      https://gitlab.com/apinephp/resolver
 * @copyright Copyright (c) 2018-2019 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/resolver/blob/master/LICENSE (MIT License)
 */

/** @noinspection PhpUnhandledExceptionInspection */
/** @noinspection PhpUnusedLocalVariableInspection */

declare(strict_types=1);

use Apine\Resolver\ObjectConstructorResolver;
use Apine\Resolver\ResolverInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

class ObjectConstructorResolverTest extends TestCase
{
    private function buildResolver(): ResolverInterface
    {
        /** @var MockObject|ContainerInterface $container */
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        return new ObjectConstructorResolver($container);
    }
    
    public function testGetSetArguments(): void
    {
        $arguments = [
            'name' => 'Merlin'
        ];
        $resolver = $this->buildResolver();
        
        $resolver->setArguments($arguments);
        $this->assertEquals($arguments, $resolver->getArguments());
    }
    
    /**
     * @expectedException \TypeError
     * @noinspection PhpParamsInspection
     */
    public function testSetArgumentsWithBadType(): void
    {
        $resolver = $this->buildResolver();
        $resolver->setArguments('bonjour');
    }
    
    public function testGetSetDefaultValue(): void
    {
        $defaultValue = 'default';
        
        $resolver = $this->buildResolver();
        $resolver->setDefaultValue($defaultValue);
        
        $this->assertEquals($defaultValue, $resolver->getDefaultValue());
    }
    
    public function testHasArgument(): void
    {
        $arguments = [
            'name' => 'Merlin'
        ];
        $resolver = $this->buildResolver();
        $resolver->setArguments($arguments);
        
        $this->assertTrue($resolver->hasArgument('name'));
        $this->assertFalse($resolver->hasArgument('target'));
    }
    
    public function testGetArgument(): void
    {
        $arguments = [
            'name' => 'Merlin'
        ];
        $resolver = $this->buildResolver();
        $resolver->setArguments($arguments);
        
        $this->assertEquals('Merlin', $resolver->getArgument('name'));
    }
    
    public function testGetArgumentWhenArgumentNotSetReturnDefaultValue(): void
    {
        $defaultValue = 'default';
        $arguments = [
            'name' => 'Merlin'
        ];
        $resolver = $this->buildResolver();
        $resolver->setArguments($arguments);
        
        $this->assertEquals(null, $resolver->getArgument('target'));
        
        $resolver->setDefaultValue($defaultValue);
        $this->assertEquals($defaultValue, $resolver->getArgument('target'));
    }
    
    public function testResolveWithoutConstructor(): void
    {
        /** @var MockObject|ContainerInterface $container */
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        
        $resolver = new ObjectConstructorResolver($container);
        $object = $resolver->resolve(ObjectConstructorResolverTestClassNoConstructor::class);
        
        $this->assertInstanceOf(ObjectConstructorResolverTestClassNoConstructor::class, $object);
    }
    
    public function testResolveWithConstructor(): void
    {
        /** @var MockObject|ContainerInterface $container */
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $container->method('has')->with('request')->willReturn(true);
        $container->method('get')->with('request')->willReturn($this->getMockForAbstractClass(TestClassInterface::class));
        
        $resolver = new ObjectConstructorResolver($container);
        $object = $resolver->resolve(ObjectConstructorResolverTestClass::class);
        
        $this->assertInstanceOf(ObjectConstructorResolverTestClass::class, $object);
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessageRegExp /Class (.*?) does not exist/
     */
    public function testResolveWhenClassNotExist(): void
    {
        /** @var MockObject|ContainerInterface $container */
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        
        $resolver = new ObjectConstructorResolver($container);
        $object = $resolver->resolve('ThisIsTotallyTheNameOfAClass');
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessageRegExp /Cannot create instance of (.*?)/
     */
    public function testResolveWhenWrongType(): void
    {
        /** @var MockObject|ContainerInterface $container */
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        
        $resolver = new ObjectConstructorResolver($container);
        $object = $resolver->resolve(false);
    }
    
    public function testResolveWithArguments(): void
    {
        /** @var MockObject|ContainerInterface $container */
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $container
            ->method('has')
            ->willReturnOnConsecutiveCalls(
                [
                    $this->equalTo('response'),
                    'cat'
                ]
            )->willReturnOnConsecutiveCalls(
                [
                    true,
                    false
                ]
            );
        $container
            ->method('get')
            ->with($this->equalTo('response'))
            ->willReturn($this->getMockForAbstractClass(TestClassInterface::class));
        
        $resolver = new ObjectConstructorResolver($container);
        $resolver->setArguments([
            'name' => 'Something',
        ]);
        
        $result = $resolver->resolve('ObjectConstructorResolverTestClassWithArguments');
        
        $this->assertEquals('Something', $result->name);
        $this->assertInstanceOf(TestClassInterface::class, $result->response);
    }
    
    public function testResolveWhenArgumentNotPassedWillUseDefaultValue(): void
    {
        /** @var MockObject|ContainerInterface $container */
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $container
            ->method('has')
            ->willReturnOnConsecutiveCalls(
                [
                    $this->equalTo('response'),
                    $this->equalTo('name')
                ]
            )->willReturnOnConsecutiveCalls(
                [
                    true,
                    false
                ]
            );
        $container
            ->method('get')
            ->with($this->equalTo('response'))
            ->willReturn($this->getMockForAbstractClass(TestClassInterface::class));
        
        $resolver = new ObjectConstructorResolver($container);
        $response = $resolver->resolve('ObjectConstructorResolverTestClassWithArguments');
        
        $this->assertEquals('Merlin', $response->name);
        $this->assertInstanceOf(TestClassInterface::class, $response->response);
    }
}