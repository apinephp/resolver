<?php
/**
 * APIne Dependency Resolver
 *
 * @link      https://gitlab.com/apinephp/resolver
 * @copyright Copyright (c) 2018-2019 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/resolver/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Resolver;

use InvalidArgumentException;
use ReflectionClass;
use ReflectionMethod;

use Throwable;
use function is_string, gettype;

/**
 * Class ObjectConstructorResolver
 *
 * @package Apine\Resolver
 */
class ObjectConstructorResolver extends AbstractResolver implements ObjectConstructorResolverInterface
{
    use ArgumentsAwareTrait;
    
    /**
     * Create an instance of a class applying automatic
     * dependency resolving and injection
     *
     * @param class-string|object $toResolve Class name
     *
     * @return object
     * @throws \ReflectionException
     */
    public function resolve($toResolve)
    {
        try {
            $reflectionClass = new ReflectionClass($toResolve);
        } catch (Throwable $e) {
            if (is_string($toResolve)) {
                throw new InvalidArgumentException(sprintf('Class %s does not exist', $toResolve));
            }
    
            throw new InvalidArgumentException(sprintf('Cannot create instance of %s', gettype($toResolve)));
        }
    
        $reflectionMethod = $reflectionClass->getConstructor();
    
        if ($reflectionMethod instanceof ReflectionMethod) {
            $arguments = $this->parseArguments($this->getContainer(), $reflectionMethod);
            return $reflectionClass->newInstanceArgs($arguments);
        }
        
        return $reflectionClass->newInstance();
    }
}