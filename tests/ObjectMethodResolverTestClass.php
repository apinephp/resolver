<?php
/**
 * APIne Dependency Resolver
 *
 * @link      https://gitlab.com/apinephp/resolver
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/resolver/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);


/**
 * Class ObjectMethodResolverTestClass
 */
class ObjectMethodResolverTestClass
{
    public function call(TestClassInterface $response): TestClassInterface
    {
        return $response;
    }
    
    public function callWithArguments(TestClassInterface $response, $cat = 'Merlin'): array
    {
        return [$response, $cat];
    }
}