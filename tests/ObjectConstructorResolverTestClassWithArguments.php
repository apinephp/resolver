<?php
/**
 * APIne Dependency Resolver
 *
 * @link      https://gitlab.com/apinephp/resolver
 * @copyright Copyright (c) 2019 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/resolver/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

/**
 * Class ObjectConstructorResolverTestClass
 */
class ObjectConstructorResolverTestClassWithArguments {
    public $response;
    public $name;
    
    public function __construct(TestClassInterface $response, $name = 'Merlin')
    {
        $this->response = $response;
        $this->name = $name;
    }
}