<?php
/**
 * APIne Dependency Resolver
 *
 * @link      https://gitlab.com/apinephp/resolver
 * @copyright Copyright (c) 2018-2019 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/resolver/blob/master/LICENSE (MIT License)
 */

/** @noinspection PhpDocSignatureInspection */

declare(strict_types=1);

namespace Apine\Resolver;

/**
 * Interface ResolverInterface
 */
interface ResolverInterface
{
    /**
     * @return array
     */
    public function getArguments(): array;
    
    /**
     * @param array<string,mixed> $arguments
     */
    public function setArguments(array $arguments): void;
    
    /**
     * @return mixed
     */
    public function getDefaultValue();
    
    /**
     * @param mixed $value
     */
    public function setDefaultValue($value): void;
    
    /**
     * @param string $name
     * @return bool
     */
    public function hasArgument(string $name): bool;
    
    /**
     * @param string $name
     *
     * @return mixed
     */
    public function getArgument(string $name);
    
    /**
     * @return mixed
     */
    public function resolve($toResolve);
}