<?php
/**
 * APIne Dependency Resolver
 *
 * @link      https://gitlab.com/apinephp/resolver
 * @copyright Copyright (c) 2018-2019 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/resolver/blob/master/LICENSE (MIT License)
 */

/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

use Apine\Resolver\CallableResolver;
use Apine\Resolver\ResolverInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

require_once 'CallableResolverFunction.php';
require_once 'CallableResolverFunctionWithArguments.php';

class CallableResolverTest extends TestCase
{
    private function buildResolver(): ResolverInterface
    {
        /** @var MockObject|ContainerInterface $container */
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        return new CallableResolver($container);
    }
    
    public function testGetSetArguments(): void
    {
        $arguments = [
            'name' => 'Merlin'
        ];
        $resolver = $this->buildResolver();
        
        $resolver->setArguments($arguments);
        $this->assertEquals($arguments, $resolver->getArguments());
    }
    
    /**
     * @expectedException \TypeError
     * @noinspection PhpParamsInspection
     */
    public function testSetArgumentsWithBadType(): void
    {
        $resolver = $this->buildResolver();
        $resolver->setArguments('bonjour');
    }
    
    public function testGetSetDefaultValue(): void
    {
        $defaultValue = 'default';
    
        $resolver = $this->buildResolver();
        $resolver->setDefaultValue($defaultValue);
        
        $this->assertEquals($defaultValue, $resolver->getDefaultValue());
    }
    
    public function testHasArgument(): void
    {
        $arguments = [
            'name' => 'Merlin'
        ];
        $resolver = $this->buildResolver();
        $resolver->setArguments($arguments);
        
        $this->assertTrue($resolver->hasArgument('name'));
        $this->assertFalse($resolver->hasArgument('target'));
    }
    
    public function testGetArgument(): void
    {
        $arguments = [
            'name' => 'Merlin'
        ];
        $resolver = $this->buildResolver();
        $resolver->setArguments($arguments);
        
        $this->assertEquals('Merlin', $resolver->getArgument('name'));
    }
    
    public function testGetArgumentWhenArgumentNotSetReturnDefaultValue(): void
    {
        $defaultValue = 'default';
        $arguments = [
            'name' => 'Merlin'
        ];
        $resolver = $this->buildResolver();
        $resolver->setArguments($arguments);
    
        $this->assertEquals(null, $resolver->getArgument('target'));
        
        $resolver->setDefaultValue($defaultValue);
        $this->assertEquals($defaultValue, $resolver->getArgument('target'));
    }
    
    public function testResolve(): void
    {
        /** @var MockObject|ContainerInterface $container */
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $container->method('has')->with('response')->willReturn(true);
        $container->method('get')->with('response')->willReturn($this->getMockForAbstractClass(TestClassInterface::class));
    
        $resolver = new CallableResolver($container);
        $result = $resolver->resolve('CallableResolverFunction');
    
        $this->assertInstanceOf(TestClassInterface::class, $result);
    }
    
    public function testResolveWithAnonymousFunction(): void
    {
        /** @var MockObject|ContainerInterface $container */
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $container->method('has')->with('response')->willReturn(true);
        $container->method('get')->with('response')->willReturn($this->getMockForAbstractClass(TestClassInterface::class));
        
        $resolver = new CallableResolver($container);
        $result = $resolver->resolve(static function (TestClassInterface $response) {
            return $response;
        });
        
        $this->assertInstanceOf(TestClassInterface::class, $result);
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessageRegExp /(.*?) is not a callable/
     */
    public function testResolveWhenCallableDoesNotExist(): void
    {
        /** @var MockObject|ContainerInterface $container */
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        
        $resolver = new CallableResolver($container);
        $resolver->resolve('iAmNotAFunction');
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessageRegExp /(.*?) is not a callable/
     */
    public function testResolveWhenNotCallable(): void
    {
        /** @var MockObject|ContainerInterface $container */
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        
        $resolver = new CallableResolver($container);
        $resolver->resolve([]);
    }
    
    public function testResolveWithArguments(): void
    {
        /** @var MockObject|ContainerInterface $container */
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $container
            ->method('has')
            ->willReturnOnConsecutiveCalls(
                [
                    $this->equalTo('response'),
                    'cat'
                ]
            )->willReturnOnConsecutiveCalls(
                [
                    true,
                    false
                ]
            );
        $container
            ->method('get')
            ->with($this->equalTo('response'))
            ->willReturn($this->getMockForAbstractClass(TestClassInterface::class));
    
        $resolver = new CallableResolver($container);
        $resolver->setArguments([
            'name' => 'Something',
        ]);
        
        $result = $resolver->resolve('CallableResolverFunctionWithArguments');
        
        $this->assertEquals('Something', $result[1]);
        $this->assertInstanceOf(TestClassInterface::class, $result[0]);
    }
    
    public function testResolveWhenArgumentNotPassedWillUseDefaultValue(): void
    {
        /** @var MockObject|ContainerInterface $container */
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $container
            ->method('has')
            ->willReturnOnConsecutiveCalls(
                [
                    $this->equalTo('response'),
                    'cat'
                ]
            )->willReturnOnConsecutiveCalls(
                [
                    true,
                    false
                ]
            );
        $container
            ->method('get')
            ->with($this->equalTo('response'))
            ->willReturn($this->getMockForAbstractClass(TestClassInterface::class));
    
        $resolver = new CallableResolver($container);
        $response = $resolver->resolve('CallableResolverFunctionWithArguments');
    
        $this->assertEquals('Merlin', $response[1]);
        $this->assertInstanceOf(TestClassInterface::class, $response[0]);
    }
}
