<?php
/**
 * APIne Dependency Resolver
 *
 * @link      https://gitlab.com/apinephp/resolver
 * @copyright Copyright (c) 2018-2019 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/resolver/blob/master/LICENSE (MIT License)
 */

/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

use Apine\Resolver\ObjectMethodResolver;
use Apine\Resolver\ResolverInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

class ObjectMethodResolverTest extends TestCase
{
    private function buildResolver(): ResolverInterface
    {
        /** @var MockObject|ContainerInterface $container */
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        return new ObjectMethodResolver($container, ObjectMethodResolverTestClass::class);
    }
    
    public function testGetSetArguments(): void
    {
        $arguments = [
            'name' => 'Merlin'
        ];
        $resolver = $this->buildResolver();
        
        $resolver->setArguments($arguments);
        $this->assertEquals($arguments, $resolver->getArguments());
    }
    
    /**
     * @expectedException \TypeError
     * @noinspection PhpParamsInspection
     */
    public function testSetArgumentsWithBadType(): void
    {
        $resolver = $this->buildResolver();
        $resolver->setArguments('bonjour');
    }
    
    public function testGetSetDefaultValue(): void
    {
        $defaultValue = 'default';
        
        $resolver = $this->buildResolver();
        $resolver->setDefaultValue($defaultValue);
        
        $this->assertEquals($defaultValue, $resolver->getDefaultValue());
    }
    
    public function testHasArgument(): void
    {
        $arguments = [
            'name' => 'Merlin'
        ];
        $resolver = $this->buildResolver();
        $resolver->setArguments($arguments);
        
        $this->assertTrue($resolver->hasArgument('name'));
        $this->assertFalse($resolver->hasArgument('target'));
    }
    
    public function testGetArgument(): void
    {
        $arguments = [
            'name' => 'Merlin'
        ];
        $resolver = $this->buildResolver();
        $resolver->setArguments($arguments);
        
        $this->assertEquals('Merlin', $resolver->getArgument('name'));
    }
    
    public function testGetArgumentWhenArgumentNotSetReturnDefaultValue(): void
    {
        $defaultValue = 'default';
        $arguments = [
            'name' => 'Merlin'
        ];
        $resolver = $this->buildResolver();
        $resolver->setArguments($arguments);
        
        $this->assertEquals(null, $resolver->getArgument('target'));
        
        $resolver->setDefaultValue($defaultValue);
        $this->assertEquals($defaultValue, $resolver->getArgument('target'));
    }
    
    public function testSetObjectWhenArgumentIsString(): void
    {
        /** @var ContainerInterface|MockObject $container */
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $resolver = new ObjectMethodResolver($container, ObjectMethodResolverTestClass::class);
        
        $this->assertInstanceOf(ObjectMethodResolverTestClass::class, $resolver->getObject());
    }
    
    public function testSetObjectWhenArgumentIsObject(): void
    {
        /** @var ContainerInterface|MockObject $container */
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        $resolver = new ObjectMethodResolver($container, new ObjectMethodResolverTestClass());
    
        $this->assertInstanceOf(ObjectMethodResolverTestClass::class, $resolver->getObject());
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessageRegExp /Cannot create instance of (.*?)/
     */
    public function testSetObjectWhenArgumentWrongType(): void
    {
        /** @var ContainerInterface|MockObject $container */
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        new ObjectMethodResolver($container, 345089);
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessageRegExp /Class (.*?) does not exist/
     */
    public function testSetObjectWhenArgumentIsStringAndClassNotExists(): void
    {
        /** @var ContainerInterface|MockObject $container */
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
        new ObjectMethodResolver($container, 'NotAClass');
    }
    
    public function testResolve(): void
    {
        /** @var ContainerInterface|MockObject $container */
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
    
        $container
            ->method('has')
            ->willReturnOnConsecutiveCalls(
                [
                    $this->equalTo('response'),
                    'cat'
                ]
            )->willReturnOnConsecutiveCalls(
                [
                    true,
                    false
                ]
            );
        $container
            ->method('get')
            ->with($this->equalTo('response'))
            ->willReturn($this->getMockForAbstractClass(TestClassInterface::class));
        
        $resolver = new ObjectMethodResolver($container, ObjectMethodResolverTestClass::class);
        $result = $resolver->resolve('call');
        
        $this->assertInstanceOf(TestClassInterface::class, $result);
    }
    
    /**
     * @expectedException \RuntimeException
     * @expectedExceptionMessageRegExp /Method (.*?) not found in class (.*?)/
     */
    public function testResolveWhenMethodNotInObject(): void
    {
        /** @var ContainerInterface|MockObject $container */
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
    
        $resolver = new ObjectMethodResolver($container, ObjectMethodResolverTestClass::class);
        $resolver->resolve('something');
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessageRegExp /Method name must be a string/
     * @noinspection PhpParamsInspection
     */
    public function testResolveWhenMethodNotString(): void
    {
        /** @var ContainerInterface|MockObject $container */
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
    
        $resolver = new ObjectMethodResolver($container, ObjectMethodResolverTestClass::class);
        $resolver->resolve([]);
    }
    
    public function testResolveWithArguments(): void
    {
        /** @var ContainerInterface|MockObject $container */
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
    
        $container
            ->method('has')
            ->willReturnOnConsecutiveCalls(
                [
                    $this->equalTo('response'),
                    'cat'
                ]
            )->willReturnOnConsecutiveCalls(
                [
                    true,
                    false
                ]
            );
        $container
            ->method('get')
            ->with($this->equalTo('response'))
            ->willReturn($this->getMockForAbstractClass(TestClassInterface::class));
    
        $resolver = new ObjectMethodResolver($container, ObjectMethodResolverTestClass::class);
        $resolver->setArguments([
            'cat' => 'Grisgris'
        ]);
        $results = $resolver->resolve('callWithArguments');
    
        $this->assertInstanceOf(TestClassInterface::class, $results[0]);
        $this->assertEquals('Grisgris', $results[1]);
    }
    
    public function testResolveWhenArgumentNotPassedWillUseDefaultValue(): void
    {
        /** @var ContainerInterface|MockObject $container */
        $container = $this->getMockForAbstractClass(ContainerInterface::class);
    
        $container
            ->method('has')
            ->willReturnOnConsecutiveCalls(
                [
                    $this->equalTo('response'),
                    'cat'
                ]
            )->willReturnOnConsecutiveCalls(
                [
                    true,
                    false
                ]
            );
        $container
            ->method('get')
            ->with($this->equalTo('response'))
            ->willReturn($this->getMockForAbstractClass(TestClassInterface::class));
    
        $resolver = new ObjectMethodResolver($container, ObjectMethodResolverTestClass::class);
        $results = $resolver->resolve('callWithArguments');
        
        $this->assertInstanceOf(TestClassInterface::class, $results[0]);
        $this->assertEquals('Merlin', $results[1]);
    }
}