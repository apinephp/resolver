<?php
/**
 * APIne Dependency Resolver
 *
 * @link      https://gitlab.com/apinephp/resolver
 * @copyright Copyright (c) 2018-2019 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/resolver/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Resolver;

use InvalidArgumentException;
use ReflectionFunction;

use Throwable;
use function gettype;
use function sprintf, ucfirst;

/**
 * Class CallableResolver
 */
class CallableResolver extends AbstractResolver implements CallableResolverInterface
{
    use ArgumentsAwareTrait;
    
    /**
     * Execute a callable applying automatic
     * dependency resolving and injection
     *
     * @param \Closure|callable-string $toResolve
     *
     * @return mixed
     * @throws \ReflectionException
     */
    public function resolve($toResolve)
    {
        try {
            $reflectionFunction = new ReflectionFunction($toResolve);
        } catch (Throwable $e) {
            throw new InvalidArgumentException(sprintf('%s is not a callable', ucfirst(gettype($toResolve))));
        }
    
        $arguments = $this->parseArguments($this->getContainer(), $reflectionFunction);
        return $reflectionFunction->invokeArgs($arguments);
    }
}